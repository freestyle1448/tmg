package group.tmg.view;


public interface MainView {
    void initMenu();

    void showMain();

    void showStatistics();
}
